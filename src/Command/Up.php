<?php
/**
 * Created by PhpStorm.
 * User: fabio
 * Date: 02/07/18
 * Time: 18.33
 */

namespace Dotcom\Composer\Command;


use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Up extends BaseCommand
{
    protected function configure()
    {
        $this->setName('up');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Executing');
    }
}